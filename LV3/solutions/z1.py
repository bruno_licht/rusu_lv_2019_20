import pandas as pd
import sys

data=pd.read_csv(sys.path[0]+"/../resources/mtcars.csv")
print(data.sort_values("mpg").tail(5).car)
print(data[(data.cyl==8)].sort_values("mpg").head(3).car)
print(data[(data.cyl==6)]["mpg"].mean())
print(data[(data.cyl==4) & (data.wt>=2) & (data.wt<=2.2)]["mpg"].mean())
print("Manual: "+str(len(data[data.am==0]))+" Automatic: "+str(len(data[data.am==1])))
print(len(data[(data.am==1) & (data.hp>=100)]))
print(data.wt*1000/2.2046)