import pandas as pd
import sys
import matplotlib.pyplot as plt 

data=pd.read_csv(sys.path[0]+"/../resources/mtcars.csv")
  
fig = plt.figure(figsize = (10, 5))
#plt.bar(['4','6','8'], [len(data[data.cyl==4]),len(data[data.cyl==6]),len(data[data.cyl==8])], color ='maroon',width = 0.4)
#plt.show()
#plt.boxplot(x=(data[data.cyl==4].wt.array,data[data.cyl==6].wt.array,data[data.cyl==8].wt.array),labels=("4","6","8"))
#plt.show()
#plt.bar(["Manual","Automatic"], [data[data.am==0].mpg.mean(),data[data.am==1].mpg.mean()], color ='maroon',width = 0.4)
#plt.show()
plt.plot(data[data.am==0].hp.array,data[data.am==0].qsec.array,'r+')
plt.plot(data[data.am==1].hp.array,data[data.am==1].qsec.array,'b*')
plt.show()