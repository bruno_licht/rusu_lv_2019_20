# prvo smo učitali datoteku te smo koristili metodu findall kako bi prema odgovarajućem regularnom izrazu
# dohvatili sve email adrese(u ovom slucaju jedini kriterij je bio postojanje barem jednog znaka lijevo i desno od '@')
# i za kraj smo pri ispisu uklonili nepotrebne znakove koji su dohvaćeni zbog jednostavnosti regexa
import re

datoteka = open("mbox-short.txt")

adrese = re.findall('\S+@\S+', datoteka.read())
for adresa in adrese:
    print(adresa.split('@')[0].lstrip('<'))
