#slično kao i prošli zadatak stvaramo nasumične vrijednosti i prikazujemo ih na histogramu
# bins predstavlja broj stupaca u histogramu

import numpy as np
import matplotlib.pyplot as plt

v = np.random.randint(1, 7, size=100)
plt.hist(v, bins=6)
