# prvo smo pomocu numpya stvorili vektor popunjen nulama i jedinicama, nakon toga smo nova dva vektora koji sadrže visine
# popunili normalnom distribucijom s predefiniranim parametrima, zatim pomoću histograma prikazujemo ispunjene vektore
# odgovarajucim bojama i postavljamo nazive osi, za kraj računamo prosječnu vrijednost visine svakog vektora i prikazujemo
# je kao ravnu liniju na grafu

from importlib.metadata import distribution
import numpy as np
import matplotlib.pyplot as plt

numbers=np.random.randint(2,size=10)
miM,sigmaM=180,7
miF,sigmaF=167,7
distributionM, distributionF=[],[]

for num in numbers:
    if(num==1):
        distributionM.extend(np.random.normal(miM,sigmaM,1))
    else:
        distributionF.extend(np.random.normal(miF,sigmaF,1))

plt.hist([distributionM,distributionF],color=['blue','red'])
plt.xlabel("Visina")
plt.ylabel("Broj pojedinaca")
plt.title("Distribucija visine")

heightAM=np.average(distributionM)
heightAF=np.average(distributionF)
plt.axvline(heightAM,color="blue")
plt.axvline(heightAF,color="red")


