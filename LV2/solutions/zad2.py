#slično kao i prijašnji zadatak samo što smo u ovom slučaju koristili 5 različitih regexa koji odgovaraju traženim kriterijima
# možda najbitnija stvar za napomenuti pri rješavanju je uporaba uglatih zagrada kako bi se specificirao niz znakova
# kako bi se regex pojednostavio
import re
datoteka = open("mbox-short.txt")
ucitano = datoteka.read()

adrese=re.findall('\S+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}',ucitano)
for i in range(len(adrese)):
    adrese[i]=adrese[i].split('@')[0]
dat=' '.join(adrese)
adrese1 = re.findall('\s\S+[Aa]+\S*\s', dat)
adrese2 = re.findall('\s[B-Zb-z.0-9]*[Aa][B-Zb-z.0-9]*\s', dat)
adrese3 = re.findall('\s[B-Zb-z.0-9]+\s', dat)
adrese4 = re.findall('\s\S*[0-9]+\S*\s', dat)
adrese5 = re.findall('\s[a-z]*\s', dat)
for adresa in adrese1:
    print(adresa.lstrip(' ').lstrip('<'))
