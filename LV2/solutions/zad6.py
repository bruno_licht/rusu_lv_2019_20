#pomoću mpimg ucitamo sliku i jer je riječ o objektu ako želimo duplicirati sliku moramo koristiti metodu copy(umjesto
# jednostavnog novaslika=slika koji bi proveo plitko kopiranje), nakon toga možemo samo pomnožiti sliku s nekom vrijednosti
# pošto je riječ o grayscale matrici kako bi dobili svjetliju sliku i nakon toga pomoću imshow i figure prikazujemo više slika 
# na ekranu

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

image=mpimg.imread('tiger.png')
newImg=image.copy()
brightnessMultiplier=1.33
newImg*=brightnessMultiplier
imgPlot=plt.imshow(image)
plt.figure()
newImgPlot=plt.imshow(newImg)