# korištenjem pandas biblioteke uspjeli smo učitati csv datoteku(moguće je i sa np ali nakon koristenja dostupnih metoda potrebno
# je daljnja obrada podataka prije moguće uporabe), pomocu plt.scatter metode prikazujemo ovisnost konjske snage o potrošnji
# dok c parametar služi za bojanje točaka na grafu što bi trebalo prestavljat težinu automobila(ljubičasta -niska vrijednost,
# žuta visoka vrijednost)

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

cars=pd.read_csv('mtcars.csv')
plt.scatter(cars.hp, cars.mpg, c=cars.wt)
plt.xlabel("Konjska snaga - HP")
plt.ylabel("Potrošnja - MPG")

print("Minimalna potrošnja:", min(cars.mpg))
print("Maksimalna potrošnja:", max(cars.mpg))
print("Potrošnja potrošnja:", np.average(cars.mpg))