U prva dva zadatka smo generirali dva nasumična niza podataka te pomoću dane funkcije smo trening podatke prikazali na grafu ovisno o njihovoj klasi.
U trećem zadataku je pomoću trening podataka izrađen model logističke regresije koji kada se prikaže grafički daje granicu dviju klasa. U četvrtom zadatku je to isto napravljeno samo pomoću gradijenta
U petom zadatku smo modelu logističke regresije dali testne podatke za klasifikaciju te smo rezultat prikazali grafički.
U šestom zadatku smo izračunavali metriku(preciznost, točnost...) klasifikacije testnih podataka pomoću matrice zabune.
U sedmom zadatku smo ponovoli sve prijašnje zadatke samo što smo trening podatke proširili kako bi se dobila polinomska transformacija te smo tako dobili bolju klasifikaciju.
U zadnjem zadatku je izrađen model K najbližih susjeda.
