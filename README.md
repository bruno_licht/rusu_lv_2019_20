# Raspoznavanje uzoraka i strojno učenje

Ak.godina 2019./2020.

## Laboratorijske vježbe

Ovaj repozitorij sadrži potrebne datoteke za izradu svake laboratorijske vježbe (npr. LV1/resources). Rješenje svake laboratorijske vježbe treba spremiti u odgovarajući direktorij (npr. LV1/solutions).

## Podaci o studentu:

Ime i prezime: Bruno Licht

Promjene u counting_words.py:
Raw_input zamijenjen sa input naredbom, u prvom printu je ispravljeno ime varijable,
u for petlji koja prelazi kroz riječi u redu sam za else blok promijenio da se poveća vrijednost odgovarajućeg elementa
u rječniku umjesto da se postavlja na 1

Sažetak LV1 vježbe:
Na današnjim vježbama smo učili kako stvoriti vlastiti git repozitorij te na klonirati remote repozitorij. Uz to za svaki zadatak smo stvorili posebnu granu te pri završetku vježbe smo sve nove grane mergali u glavnu.
Na prva dva zadatka smo se upoznali s korištenjem naredbi unosa i izlaza, pravilnim korištenjem uvjetnih grananja,
naučili smo čitati podatke iz datoteka te ih koristiti u daljnjem radu te smo imali priliku raditi s listama i rječnicima.
Unutar svakog programa se nalazi kratki opis načina rada

Sažetak LV2 vježbe:
Na današnjim vježbama smo se upoznali sat 3 nova modula(biblioteke):
re- služi za formiranje regularnog izraza koji se koristi za pretraživanje podstringova unutar stringa
numpy- omogućuje lako stvaranje matrica i izvođenje operacija nad njima, također sadrži statističke metode poput metode za stvaranje Gaussove distribucije
matplotlib- sadrži metode koji omogućuje prikazivanje podataka u formatu veoma nalik na matlab
