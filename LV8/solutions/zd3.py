from tensorflow import keras
from keras import layers
from keras.models import Sequential
from keras.layers import Dense
from tensorflow.keras.preprocessing import image_dataset_from_directory
# ucitavanje podataka iz odredenog direktorija
train_ds = image_dataset_from_directory(directory='data/Train/',labels='inferred',label_mode='categorical',batch_size=32,image_size=(48, 48))
test_ds=image_dataset_from_directory(directory='data/Test/',labels='inferred',label_mode='categorical',batch_size=32,image_size=(48, 48))
model=Sequential([keras.Input(shape=(48,48,3)),
        layers.Conv2D(32, kernel_size=(3, 3), activation="relu"),
        layers.Conv2D(32, kernel_size=(3, 3), activation="relu"),
        layers.MaxPooling2D(pool_size=(2, 2)),
        layers.Dropout(0.2),
        layers.Conv2D(64, kernel_size=(3, 3), activation="relu"),
        layers.Conv2D(64, kernel_size=(3, 3), activation="relu"),
        layers.MaxPooling2D(pool_size=(2, 2)),
        layers.Dropout(0.2),
        layers.Conv2D(128, kernel_size=(3, 3), activation="relu"),
        layers.Conv2D(128, kernel_size=(3, 3), activation="relu"),
        layers.MaxPooling2D(pool_size=(2, 2)),
        layers.Dropout(0.2),
        layers.Flatten(),
        layers.Dense(512,activation="relu"),
        layers.Dropout(0.5),
        layers.Dense(43, activation="softmax"),
])
model.compile(optimizer="adam",loss="categorical_crossentropy",metrics=['accuracy'])
model.fit(train_ds,epochs=10,batch_size=1)
score = model.evaluate(test_ds, verbose=0)
print("Test accuracy:", score[1])
#model.save('model')

#Epoch 1/10
#1226/1226 [==============================] - 161s 130ms/step - loss: 1.0938 - accuracy: 0.7127
#Epoch 2/10
#1226/1226 [==============================] - 158s 129ms/step - loss: 0.2079 - accuracy: 0.9425
#Epoch 3/10
#1226/1226 [==============================] - 162s 132ms/step - loss: 0.1562 - accuracy: 0.9596
#Epoch 4/10
#1226/1226 [==============================] - 157s 128ms/step - loss: 0.1383 - accuracy: 0.9638
#Epoch 5/10
#1226/1226 [==============================] - 154s 126ms/step - loss: 0.1181 - accuracy: 0.9691
#Epoch 6/10
#1226/1226 [==============================] - 155s 126ms/step - loss: 0.1139 - accuracy: 0.9708
#Epoch 7/10
#1226/1226 [==============================] - 155s 126ms/step - loss: 0.1106 - accuracy: 0.9722
#Epoch 8/10
#1226/1226 [==============================] - 155s 126ms/step - loss: 0.1115 - accuracy: 0.9725
#Epoch 9/10
#1226/1226 [==============================] - 159s 129ms/step - loss: 0.0991 - accuracy: 0.9745
#Epoch 10/10
#1226/1226 [==============================] - 160s 130ms/step - loss: 0.1169 - accuracy: 0.9719
#Test accuracy: 0.9646872282028198