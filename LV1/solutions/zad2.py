#unosi se broj te se provjerava gdje se nalazi unutar intervala(ako se uopće nalazi), u slučaju da nije unesen broj
# u programu će se javiti greška koju će uhvatiti try naredba te se izvršavaju naredbe u except bloku
try:
  broj = float(input("Unesite broj od 0 do 1:"))
  if broj > 1 or broj < 0:
    print("Broj nije unutar intervala.")
  elif broj >= 0.9:
    print("A")
  elif broj >= 0.8:
    print("B")
  elif broj >= 0.7:
    print("C")
  elif broj >= 0.6:
    print("D")
  else:
    print("F")
except:
  print("Rekao sam broj")