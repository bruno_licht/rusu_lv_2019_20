#otvaramo datoteku te ju čitamo red po red dok ne dodemo do kraja
#sadržaj svakog retka smo odvojili po razmaku te uspoređivali da li se u redu nalazi traženi string

name = input("Upisi ime datoteke: ")
total = 0
counter = 0
average = 0
with open(name) as f:
    for line in f:
        if line.split(" ")[0]=="X-DSPAM-Confidence:":
            total += float(line.split(" ")[1])
            counter += 1
average = total / counter
print('Average X-DSPAM-Confidence: ', average)
